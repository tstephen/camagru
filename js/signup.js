// Select sign up link
var signUp = document.getElementById("signUpBtn");
// Select sign up modal
var signupModal = document.getElementById("signupModal");
// Get close button
var closeBtn = document.getElementById("closeBtn2");

// Listen for a click on Sign Up
signUp.addEventListener('click', openModal);

// Function to open modal
function openModal() {
	signupModal.style.display = 'block'; 
}

// Listen for click on closeBtn2
closeBtn.addEventListener('click', closeModal);

// Function to close modal
function closeModal() {
	signupModal.style.display = 'none';
}

// Listen for clicks outside of modal
window.addEventListener('click', clickOutside);

// Function to close if clicked outside modal
function clickOutside(e) {
	if (e.target == signupModal) {
		signupModal.style.display = 'none'; 
	}
}

// Send SignUp request to controller
var btnSignin = document.getElementsByClassName("signupBtn");
var changeMe = document.getElementById("changeMe");
var usernameSignin = document.getElementById("signup-username");
var passwordSignin = document.getElementById("signup-password");
var emailSignin = document.getElementById("signup-email");

btnSignin[0].addEventListener('click', function() {
	
	if (!usernameSignin.value || !passwordSignin.value || !emailSignin.value) {
		changeMe.innerHTML = "Enter a valid username, password and email!";
		return ;
	}

 	const xhr = new XMLHttpRequest();
	xhr.onload = function() {
		// Modify content
		changeMe.innerHTML = this.responseText;
	}

	xhr.open("POST", "php/confirmSignUp.php", true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send("username=" + usernameSignin.value + "&password=" + passwordSignin.value + "&email=" + emailSignin.value);
});