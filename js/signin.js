// Get the modal element
var modal = document.getElementById("simpleModal");
// Get open modal button
var modalBtn = document.getElementById("loginBtn");
// Get close button
var closeBtn = document.getElementById("closeBtn");

// Listen for a click on Login
modalBtn.addEventListener('click', openModal);

// Function to open modal
function openModal() {
	modal.style.display = 'block'; 
}

// Listen for click on closeBtn
closeBtn.addEventListener('click', closeModal);

// Function to close modal
function closeModal() {
	modal.style.display = 'none';
}

// Listen for clicks outside of modal
window.addEventListener('click', clickOutside);

// Function to close if clicked outside modal
function clickOutside(e) {
	if (e.target == modal) {
		modal.style.display = 'none'; 
	}
}

// Send login request to controller
var btn = document.getElementsByClassName("btn");
var changeMe = document.getElementById("changeMe");
var username = document.getElementById("login-username");
var password = document.getElementById("login-password");

btn[0].addEventListener('click', function() {
 	const xhr = new XMLHttpRequest();
	xhr.onload = function() {
		// Modify content	
		changeMe.innerHTML = this.responseText;
		closeModal();
	}

	xhr.open("POST", "php/confirmLogin.php", true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send("username=" + username.value + "&password=" + password.value);
});
