<?php
	include_once("User.class.php");

	$username = $_POST["username"];
	$passwd = $_POST["password"];
	$email = $_POST["email"];

	function validEmail($email) {
		if (preg_match("/^.+@.+\..+$/", $email))
			return True;
		return False;
	}

	$usr = new User;
	if ($usr->isExistingUser($username, $email)) {
		echo "Username or email is already taken <br />";
	} else {
		if (validEmail($email))
			$usr->addUser($username, $passwd, $email);
		else
			echo "Email address is not valid <br />";
	}
?>