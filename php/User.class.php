<?php
	session_start();
	include_once("../config.php");

	class User {
		private $conn;

		public function __construct() {
			$this->conn = getDB();
		}

		public function isExistingUser($username, $email) {
			$stmt = $this->conn->prepare("SELECT `username`,`email` FROM `Users` WHERE (`username` LIKE :username) OR (`email` LIKE :email)");
			$stmt->bindParam(":username", $username, PDO::PARAM_STR);
			$stmt->bindParam(":email", $email, PDO::PARAM_STR);
			$stmt->execute();

			$matchingUsers = array();
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$matchingUsers[] = $row;
			}

			if (count($matchingUsers) != 0) {
				return True;
			}
			return False;
		}

		public function verifyUser($username, $password) {
			try {
				$stmt = $this->conn->prepare("SELECT `uid`,`password` FROM `Users` WHERE (`username` LIKE :username)");
				$stmt->bindParam(":username", $username, PDO::PARAM_STR);
				$stmt->execute();
	
				$matchingUsers = array();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$matchingUsers[] = $row;
				}
	
				if (count($matchingUsers) != 1)
					return false;
				foreach ($matchingUsers as $row) {
					if (password_verify($password, $row["password"]))
					{
						$_SESSION['uid'] = $row['uid'];
						return true;
					}
				}
				return false;
			} catch (PDOException $e) {
				echo '{"error":{"text":'. $e->getMessage() .'}}';
			}
		}

		function addUser($username, $password, $email) {
			try {
				$hashed_password = password_hash($password, PASSWORD_DEFAULT);
				$stmt = $this->conn->prepare("INSERT INTO `Users`(`username`, `password`, `email`)
						VALUES (:username, :hashed_password, :email)");		
				$stmt->bindParam(":username", $username, PDO::PARAM_STR);
				$stmt->bindParam(":hashed_password", $hashed_password, PDO::PARAM_STR);
				$stmt->bindParam(":email", $email, PDO::PARAM_STR);	
				$stmt->execute();
				echo "Succesfully added user: " . $username . "<br />";
			} catch (PDOException $e) {
				echo "Error: " . $e->getMessage() . "<br />";
			}
		}

		public function __destruct() {
			$this->conn = null;
		}
	}
?>
