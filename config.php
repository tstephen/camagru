<?php
	session_start();

	/* DATABASE CONFIGURATION */
	define("DB_SERVER", "localhost:3306");
	define("DB_USERNAME", "tstephen");
	define("DB_PASSWORD", "123456");
	define("DB_DATABASE", "db_camagru");
	define("BASE_URL", "http://localhost:8889/Camagru/");

	function createInitialDatabase() {
		$dbhost = DB_SERVER;
		$dbname = DB_DATABASE;
	
		try {
			$conn = new PDO("mysql:host=$dbhost", DB_USERNAME, DB_PASSWORD);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "CREATE DATABASE $dbname";
			$conn->exec($sql);
			echo "Database created successfully<br/>";
		} catch (PDOException $e) {
			echo "Database creation failed: " . $e->getMessage() . "<br/>";
		}
	}

	function getDB() {
		$dbhost = DB_SERVER;
		$dbname = DB_DATABASE;
	
		try {
			$dbConnection = new PDO("mysql:host=$dbhost;dbname=$dbname;", DB_USERNAME, DB_PASSWORD);
			$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$dbConnection->exec("SET NAMES utf8");
			return $dbConnection;
		} catch (PDOException $e) {
			echo "Connection failed: " . $e->getMessage() . "<br/>";	
		}
	}

	function createUsersTable($conn, $name) {
		try {
			$sql = "CREATE TABLE $name (
				`uid` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				`password` VARCHAR(200) NOT NULL,
				`username` VARCHAR(30) NOT NULL,
				`email` VARCHAR(80) NOT NULL,
				`firstName` VARCHAR(80),
				`lastName` VARCHAR(80),
				`verified` BOOLEAN DEFAULT FALSE
				)";
			$conn->exec($sql);
			echo "Table Users created successfully<br/>";
		} catch (PDOException $e) {
			echo "Error creating table: " . $e->getMessage() . "<br/>";
		}
	}
?>