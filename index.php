<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="css/signin.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="css/signup.css" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<title>Camagru | Image Sharing</title>
</head>
<body>
	<nav class="topNavBar">
		<div id="loginBtn" class="navPanel" style="grid-column:2;">
			<a href="#0" class="navLinks">Login</a>
		</div>
		<div class="toHome">
			<a href="index.php" class="navLinks">Camagru</a>
		</div>
		<div id="signUpBtn" class="navPanel" style="grid-column:4;">
			<a href="#0" class="navLinks">Sign Up</a>
		</div>
	</nav>
	<div id="simpleModal" class="modal">
		<div class="modal-content">
			<span id="closeBtn">&times;</span>
			<div class="login-box">
				<h1>Sign In</h1>
				<div class="textbox userTextbox">
					<input id="login-username" type="text" placeholder="Username" name="" value="" style="border:none;outline:none;background:none;font-size:25px;color: white;"/>
				</div>
				<div class="textbox passTextbox">
					<input id="login-password" type="password" placeholder="Password" name="" value="" style="border:none;outline:none;background:none;font-size:25px;color: white;"/>
				</div>
				<div>
					<input class="btn" type="button" name="" value="Sign In"/>
				</div>
			</div>
		</div>
	</div>
	<div id="signupModal" class="modal">
		<div class="modal-content-signup">
				<span id="closeBtn2">&times;</span>
				<div class="signup-box">
					<h1>Sign Up</h1>
					<div class="textbox userName">
						<input id="signup-username"type="text" placeholder="Username" name="" value="" style="border:none;outline:none;background:none;font-size:25px;color: white;"/>
					</div>
					<div class="textbox passWd">
						<input id="signup-password" type="password" placeholder="Password" name="" value="" style="border:none;outline:none;background:none;font-size:25px;color: white;"/>
					</div>
					<div class="textbox email">
						<input id="signup-email" type="text" placeholder="Email" name="" value="" style="border:none;outline:none;background:none;font-size:25px;color: white;"/>
					</div>
					<div>
						<input class="signupBtn" type="button" name="" value="Sign Up"/>
					</div>
				</div>
		</div>
	</div>
	<p class="" id="changeMe"></p>
	<style>
		#changeMe {
			position: absolute;
			top: 200px;
			left: 100px;
		}
	</style>
	<script type="text/javascript" src="js/signup.js"></script>
	<script type="text/javascript" src="js/signin.js"></script>	
</body>
</html>
